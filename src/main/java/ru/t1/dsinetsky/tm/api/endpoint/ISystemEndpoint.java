package ru.t1.dsinetsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.request.ServerAboutRequest;
import ru.t1.dsinetsky.tm.dto.request.ServerVersionRequest;
import ru.t1.dsinetsky.tm.dto.response.ServerAboutResponse;
import ru.t1.dsinetsky.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull ServerAboutResponse getAbout(@NotNull final ServerAboutRequest serverAboutRequest);

    @NotNull ServerVersionResponse getVersion(@NotNull final ServerVersionRequest serverVersionRequest);

}
