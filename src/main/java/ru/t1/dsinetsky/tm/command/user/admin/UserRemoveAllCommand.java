package ru.t1.dsinetsky.tm.command.user.admin;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class UserRemoveAllCommand extends AbstractAdminCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_DELETE_ALL_USER;

    @NotNull
    public static final String DESCRIPTION = "Deletes all users from system but administrator";

    @Override
    public void execute() throws GeneralException {
        getUserService().clear();
        System.out.println("Users successfully deleted!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
