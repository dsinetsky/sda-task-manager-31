package ru.t1.dsinetsky.tm.command.user.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserUpdateNameByIdCommand extends AbstractAdminCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_UPDATE_USER_BY_ID;

    @NotNull
    public static final String DESCRIPTION = "Updates first, middle and last name of user (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of user:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("Enter new first name:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter new last name:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter new middle name:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        getUserService().updateUserById(id, firstName, lastName, middleName);
        System.out.println("User name successfully updated!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
