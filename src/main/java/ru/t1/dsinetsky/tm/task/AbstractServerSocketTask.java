package ru.t1.dsinetsky.tm.task;


import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    public AbstractServerSocketTask(final @NotNull Server server, @NotNull final Socket socket) {
        super(server);
        this.socket = socket;
    }
}
