package ru.t1.dsinetsky.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.component.Server;
import ru.t1.dsinetsky.tm.dto.request.AbstractRequest;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public final class ServerRequestTask extends AbstractServerSocketTask {

    public ServerRequestTask(final @NotNull Server server, final @NotNull Socket socket) {
        super(server, socket);
    }

    @Override
    @SneakyThrows
    public void run() {
        final InputStream inputStream = socket.getInputStream();
        final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        final Object object = objectInputStream.readObject();
        final AbstractRequest request = (AbstractRequest) object;
        @Nullable Object response = server.call(request);
        final OutputStream outputStream = socket.getOutputStream();
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket));
    }
}
