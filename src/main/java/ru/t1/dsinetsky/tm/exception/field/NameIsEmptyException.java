package ru.t1.dsinetsky.tm.exception.field;

public final class NameIsEmptyException extends GeneralFieldException {

    public NameIsEmptyException() {
        super("Name cannot be empty!");
    }

}
