package ru.t1.dsinetsky.tm.exception.entity;

import ru.t1.dsinetsky.tm.exception.GeneralException;

public class GeneralEntityException extends GeneralException {

    public GeneralEntityException() {
    }

    public GeneralEntityException(final String message) {
        super(message);
    }

    public GeneralEntityException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GeneralEntityException(final Throwable cause) {
        super(cause);
    }

    public GeneralEntityException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
