package ru.t1.dsinetsky.tm.exception.user;

public final class RoleIsEmptyException extends GeneralUserException {

    public RoleIsEmptyException() {
        super("Role cannot be empty!");
    }

}
