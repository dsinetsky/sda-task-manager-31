package ru.t1.dsinetsky.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class ServerVersionResponse extends AbstractResponse {

    private String version;

}
