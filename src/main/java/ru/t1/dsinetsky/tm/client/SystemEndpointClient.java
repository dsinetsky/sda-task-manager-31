package ru.t1.dsinetsky.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dsinetsky.tm.dto.request.ServerAboutRequest;
import ru.t1.dsinetsky.tm.dto.request.ServerVersionRequest;
import ru.t1.dsinetsky.tm.dto.response.ServerAboutResponse;
import ru.t1.dsinetsky.tm.dto.response.ServerVersionResponse;

public class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println("Name: " + serverAboutResponse.getName() + "; E-mail: " + serverAboutResponse.getEmail() + ";");
        final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println("Version: " + serverVersionResponse.getVersion());
        client.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

}
